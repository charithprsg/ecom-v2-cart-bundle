<?php


namespace Service\Domain;


use Esol\CartBundle\Service\Domain\CartDomainService;
use PHPUnit\Framework\TestCase;

class CartServiceTest extends TestCase
{
    /** @test */
    public function create_cart_test(){
        $parameters = array('profile_id' =>'1');
        $cart = CartDomainService::createCart($parameters);
        $this->assertEquals($cart->getProfileId(),$parameters['profile_id']);

    }

}
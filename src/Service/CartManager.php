<?php

namespace Esol\CartBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Esol\CartBundle\DataModel\ReturnModel\CartManager\CartItemModel;
use Esol\CartBundle\DataModel\ReturnModel\CartManager\CartModel;
use Esol\CartBundle\Entity\Cart;
use Esol\CartBundle\Entity\Item;
use Esol\CartBundle\Service\Application\CartApplicationServiceInterface;
use Esol\ProductSearchBundle\Service\ProductSearchManagerInterface;
use Esol\ProductSearchBundle\DataModel\InputModel\ProductManager\ProductSearchParaModel;

class CartManager implements CartManagerInterface
{
    /**
     * @var CartApplicationServiceInterface
     */
    private $cartApplicationService;

    /**
     * CartManager constructor.
     */
    public function __construct(CartApplicationServiceInterface $cartApplicationService)
    {

        $this->cartApplicationService = $cartApplicationService;
    }

    public function createCart(array $parameters){

        $this->cartApplicationService->createCart($parameters);


    }

    public function getCart(int $id)
    {

    }

    public function addItems(int $cart_id,array $parameters):Item{
        $item = $this->cartApplicationService->addItems($cart_id,$parameters);
        return $item;
    }

}
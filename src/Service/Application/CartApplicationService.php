<?php


namespace Esol\CartBundle\Service\Application;


use Doctrine\ORM\EntityManagerInterface;
use Esol\CartBundle\DataModel\ReturnModel\CartManager\CartItemModel;
use Esol\CartBundle\DataModel\ReturnModel\CartManager\CartModel;
use Esol\CartBundle\Entity\Cart;
use Esol\CartBundle\Entity\Item;
use Esol\CartBundle\Service\Domain\CartDomainServiceInterface;
use Esol\CartBundle\Service\Domain\ItemDomainService;
use Esol\CartBundle\Service\Exception\InvalidCartException;
use Esol\ProductSearchBundle\Service\ProductSearchManagerInterface;

class CartApplicationService implements CartApplicationServiceInterface
{
    /**
     * @var EntityManagerInterface
     */
    private  $entityManager;
    /**
     * @var ProductSearchManagerInterface
     */
    private  $productSearchManager;

    /**
     * CartApplicationService constructor.
     */
    public function __construct(EntityManagerInterface $entityManager,ProductSearchManagerInterface $productSearchManager)
    {

        $this->entityManager = $entityManager;
        $this->productSearchManager = $productSearchManager;
    }

    public function createCart(array $parameters){
        //get profile using profile manager interface and validate
        $cart = CartDomainServiceInterface::createCart($parameters);

    }

    private function DbCreateCart(Cart $cart){
        $this->entityManager->persist($cart);
        $this->entityManager->flush();
    }

    public function getCart(int $id)
    {

    }
    public function addItems(int $cart_id,array $parameters):Item{
        $cart = $this->entityManager->getRepository(Cart::class)->find($cart_id);
        if($cart==null){
            throw new InvalidCartException();
        }
        $parameters['cart'] = $cart;
        $product = $this->productSearchManager->getProductByErpCode($parameters['erpCode']);
        $item = ItemDomainService::createItem($parameters);
        $this->entityManager->persist($item);
        $this->DbAddItem($cart,$item);
        return $item;
    }

    private function DbAddItem(Cart $cart,Item $item){
        $cart->addItem($item);
        $this->entityManager->persist($cart);
        $this->entityManager->flush();
    }
}
<?php


namespace Esol\CartBundle\Service\Application;


interface CartApplicationServiceInterface
{
    public function createCart(array $parameters);
    public function getCart(int $id);
    public function addItems(int $cart_id,array $parameters);
}
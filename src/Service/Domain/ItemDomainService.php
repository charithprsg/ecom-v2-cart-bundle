<?php


namespace Esol\CartBundle\Service\Domain;


use Esol\CartBundle\Entity\Item;
use Esol\CartBundle\Service\Domain\Exception\InvalidErpCodeException;

class ItemDomainService implements ItemDomainServiceInterface
{

    public static function createItem(array $parameters):Item
    {
        if(!isset($parameters['erpCode'])){
            throw new InvalidErpCodeException();
        }
        $item = new Item();
        $item->setCart($parameters['cart']);
        $item->setErpCode($parameters['erpCode']);
        $item->setCreatedAt(new \DateTime('now',new \DateTimeZone('Asia/Colombo')));
        $item->setIsDeleted(false);
        $item->setIsPurchased(false);
        return $item;
    }
}
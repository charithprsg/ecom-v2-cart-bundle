<?php


namespace Esol\CartBundle\Service\Domain;


interface CartDomainServiceInterface
{
    public static function createCart(array $parameters);
}
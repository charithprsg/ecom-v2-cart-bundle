<?php


namespace Esol\CartBundle\Service\Domain;


use Esol\CartBundle\Entity\Cart;
use Esol\CartBundle\Service\Domain\Exception\InvalidProfileException;

class CartDomainService implements CartDomainServiceInterface
{
    public static function createCart(array $parameters):Cart{
        if(!isset($parameters['profile_id']) || !is_numeric($parameters['profile_id'])){
            throw new InvalidProfileException();
        }
        $cart = new Cart();
        $cart->setProfileId($parameters['profile_id']);
        $cart->setCreatedAt(new \DateTime('now',new \DateTimeZone('Asia/Colombo')));

        return $cart;

    }

}
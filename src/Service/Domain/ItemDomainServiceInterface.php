<?php


namespace Esol\CartBundle\Service\Domain;


interface ItemDomainServiceInterface
{
    public static function createItem(array $parameters);
}
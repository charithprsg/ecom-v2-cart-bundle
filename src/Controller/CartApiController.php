<?php

namespace Esol\CartBundle\Controller;


use Esol\CartBundle\Service\CartManagerInterface;
use Esol\CartBundle\Serializer\CartSerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
class CartApiController
{
    /**
     * @var CartManagerInterface
     */
    private $cartManager;
    /**
     * @var CartSerializerInterface
     */
    private $cartSerializer;

    /**
     * CartApiController constructor.
     */
    public function __construct(CartManagerInterface $cartManager,CartSerializerInterface $cartSerializer)
    {
        $this->cartManager = $cartManager;
        $this->cartSerializer = $cartSerializer;
    }

    public function createCart(Request $request){
        $requestParams = json_decode($request->getContent(), true);
        $cart = $this->cartManager->createCart($requestParams);
        $serializedCart = $this->cartSerializer->serializeCartData($cart);
        return new JsonResponse(array("cart"=>$serializedCart));
    }

    public function getCart(Request $request)
    {
        $pathPara = $request->attributes->get('_route_params');
        $cart = $this->cartManager->getCart($pathPara['id']);
        $serializedCart = $this->cartSerializer->serializeCartData($cart);
        return new JsonResponse(array("cart"=>$serializedCart));
    }

    public function addItem(Request $request){
        $pathParams = $request->attributes->get('_route_params');
        $requestParams = json_decode($request->getContent(), true);
        try{
            $item = $this->cartManager->addItems($pathParams['id'],$requestParams);
            return  new JsonResponse(null,201);
        }
        catch(\Exception $e){
            dump($e);
            exit();
            return new JsonResponse(null,404);
        }


    }


}
<?php


namespace Esol\CartBundle\Listener;
use Esol\AdminBundle\Event\AdminBundleSideBarConfigurationEvent;

class CartBundleConfigurationListener
{
    private static $config = array('bundleName' => 'CartBundle','navigations'=>array('list'=>'esol.cart-list'));
    private $configuration;

    /**
     * LocationBundleConfigurationListener constructor.
     * @param $configuration
     */
    public function __construct($config)
    {
        $this->configuration = $config;
    }


    public function onConfigurationView(AdminBundleSideBarConfigurationEvent $event){
        $this->configuration = $event->addConfiguration($this->configuration);
    }

    /**
     * @return mixed
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }
}
<?php


namespace Esol\CartBundle\DataModel\ReturnModel\CartManager;


class CartItemModel
{
    public $productId;

    public $productName;

    public $productImage;

    public $mop;

    public $mrp;

    public $variation;

    public $promotion;
}
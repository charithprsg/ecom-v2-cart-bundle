<?php

namespace Esol\CartBundle\Serializer;

use Esol\CartBundle\Entity\Cart;

interface CartSerializerInterface
{
    public function serializeCartsData(\Esol\CartBundle\DataModel\ReturnModel\CartManager\CartModel $cartModel);
    public function serializeCartData(Cart $cart);
}
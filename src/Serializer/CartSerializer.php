<?php

namespace Esol\CartBundle\Serializer;

use Esol\CartBundle\Entity\Cart;

class CartSerializer implements CartSerializerInterface
{

    public function serializeCartsData(\Esol\CartBundle\DataModel\ReturnModel\CartManager\CartModel $cartModel)
    {
        $ret = array();
        $ret['id'] = $cartModel->id;
        $ret['items'] = $cartModel->items;
        $ret['itemCount'] = $cartModel->itemCount;

        return $ret;
    }


    public function serializeCartData(Cart $cart)
    {
        return 'cart';
    }
}